/**
* Created by Sergey O. Boyko on 31.01.19.
*/

#pragma once

#include <stdint.h>

/**
* List of "Certificate Transfer Service" request types
*/
enum CertificateTransferProtocol {
  /**
  * Certificate request.
  * Payload should contains the certificate identifier
  */
      CERTIFICATE_REQUEST = 0,

  /**
  * Certificate response.
  * If certificate is found successfully
  * the payload should contains public key and certificate,
  * else error code
  */
      CERTIFICATE_RESPONSE
};

/**
* Error code list
*/
enum CertificateRequestErrorCode {
  /**
  * Certificate is found successfully
  */
      NO_ERRORS_ENCOUNTERED_E = 0,

  /**
  * Certificate not found
  */
      NOT_FOUND_E
};

#pragma pack(push, 1)

/**
* Request structure (corresponding certificate_request_t)
*/
struct CertificateRequest {
  /**
  * Length of certificate identifier
  */
  uint16_t m_cert_identifier_length;

  /**
  * Certificate identifier
  */
  const char *m_cert_identifier;
};

/**
* Response structure (corresponding certificate_response_t)
*/
struct CertificateResponse {
  /**
  * May be one of the CertificateRequestErrorCode values
  */
  uint8_t m_error_code;

  /**
  * Length of the requested certificate identifier
  */
  uint16_t m_requested_cert_identifier_length;

  /**
  * Requested certificate identifier.
  * Identifier is the same as in the certificate_request_t
  */
  const char *m_requested_cert_identifier;

  /**
  * Length of the public key
  */
  uint16_t m_public_key_length;

  /**
  * Public key
  */
  const char *m_public_key;

  /**
  * Length of the certificate
  */
  uint16_t m_certificate_length;

  /**
  * Certificate
  */
  const char *m_certificate;
};

#pragma pack(pop)
