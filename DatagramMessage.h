/**
* Created by Sergey O. Boyko on 31.01.19.
*/

#pragma once

#include <stdint.h>

#pragma pack(push, 1)

/**
 * Header of the high level message.
 * Contains the information about user message
 */
struct DatagramMessageHeader {
  /**
   * Total payload length of message
   */
  uint32_t m_length;

  /**
   * Message type
   * May be one of the DatagramMessageType values
   */
  uint16_t m_type;
};

/**
* High level message
*/
struct DatagramMessage {
 /**
 * Header of message
 */
 struct DatagramMessageHeader m_header;

 /**
 * Message payload
 */
 const char *m_payload;
};

#pragma pack(pop)
