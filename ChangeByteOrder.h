/**
* Created by Sergey O. Boyko on 03.02.19.
*/

#pragma once

#include <netinet/in.h>

inline void _Htons(uint16_t *val) {
  if (val != NULL) {
    *val = htons(*val);
  }
}

inline void _Htonl(uint32_t *val) {
  if (val != NULL) {
    *val = htonl(*val);
  }
}

inline void _Ntohs(uint16_t *val) {
  if (val != NULL) {
    *val = ntohs(*val);
  }
}

inline void _Ntohl(uint32_t *val) {
  if (val != NULL) {
    *val = ntohl(*val);
  }
}
