set(NOISE_INCLUDE_DIR
        ${CMAKE_PREFIX_PATH}/include/noise
        ${CMAKE_PREFIX_PATH}/include)

set(lib_dir ${CMAKE_PREFIX_PATH}/lib/)

find_library(NOISE_KEYS noisekeys)
find_library(NOISE_PROTOBUFS noiseprotobufs)
find_library(NOISE_PROTOCOL noiseprotocol)

list(APPEND NOISE_LIBRARIES
        ${NOISE_KEYS}
        ${NOISE_PROTOBUFS}
        ${NOISE_PROTOCOL}
        )

if (NOISE_LIBRARIES)
    message("-- noise-c found")
else ()
    message("-- noise-c not found")
endif ()
