/**
* Created by Sergey O. Boyko on 02.02.19.
*/

#pragma once

/**
* Constant definitions of Security Reliable Datagram Protocol
*/
enum SrdpConstantDefinitions {
  /**
  * KEEP_ALIVE_FLAG sending interval in seconds.
  * Constant value
  */
      KEEP_ALIVE_TIMEOUT = 10,

  /**
  * Major and minor versions of SRDP
  */
      SRDP_MAJOR_VERSION = 1,
  SRDP_MINOR_VERSION = 0,


};

/**
* Variable definitions of Security Reliable Datagram Protocol
*/
enum SrdpVariableDefinitions {
  /**
  * Default, min and max values of MAX_FRAGMENT_SIZE
  */
      MAX_FRAGMENT_SIZE_DEFAULT_VALUE = 255,
  MAX_FRAGMENT_SIZE_MIN_VALUE = 1,
  MAX_FRAGMENT_SIZE_MAX_VALUE = 1000,

  /**
  * Default, min and max values in milliseconds of RETRANSMISSION_TIMEOUT
  */
      RETRANSMISSION_TIMEOUT_DEFAULT_VALUE = 200,
  RETRANSMISSION_TIMEOUT_MIN_VALUE = 50,
  RETRANSMISSION_TIMEOUT_MAX_VALUE = 1000,

  /**
  * Default, min and max values of MAX_RETRANSMISSIONS
  */
      MAX_RETRANSMISSIONS_DEFAULT_VALUE = 5,
  MAX_RETRANSMISSIONS_MIN_VALUE = 0,
  MAX_RETRANSMISSIONS_MAX_VALUE = 20,

  /**
  * Default, min and max values in milliseconds of FRAGMENT_WAITING_TIME
  */
      FRAGMENT_WAITING_TIME_DEFAULT_VALUE = 2000,
  FRAGMENT_WAITING_TIME_MIN_VALUE = 500,
  FRAGMENT_WAITING_TIME_MAX_VALUE = 25000,
};

/**
* Types of encryption protocols which SRDP supports
*/
enum SrdpEncryptionProtocol {
  /**
  * No need to encrypt channel.
  * If the flag is set m_settings_size should be '0'
  */
      NO_NEED_TO_ENCRYPT = 0,

  /**
  * Use Noise protocol (http://www.noiseprotocol.org)
  */
      NOISE_PROTOCOL = 1,
};

/**
* List of SRDP flags
*/
enum SrdpFlag {
  /**
   * Keep alive is used to verify the health of the channel.
   * Ack should be sent in response on keep alive.
   * If the flag is set packet payload should be empty
   */
      KEEP_ALIVE_FLAG = 0,

  /**
   * Synchronize sequence numbers.
   * Request is sent to start establish the UDP reliable channel.
   * If the flag is set packet payload should be empty
   */
      SYN_REQUEST_FLAG,

  /**
   * Last packet from sender.
   * If the flag is set packet payload should be empty
   */
      FIN_REQUEST_FLAG,

  /**
   * Transaction acknowledge.
   * Ack can be sent in response to one of the following situations:
   *   - receiving side accepts transaction (on of the following:
   *       keep_alive, syn_request, fin_request, upgrade_to_secure_channel_flag)
   *     (m_ack_sequence_number must contains the sequence number
   *      of the transaction packet)
   *   - receiving side successfully received the packet from fragment
   *     (m_ack_sequence_number must contains the sequence number of the successfully received packet)
   *
   * If the flag is set only m_ack_sequence_number will be read therefore
   * other fields can be empty
   */
      ACK_FLAG,

  /**
   * Current state is public key exchange
   * Payload should contains sender's public key
   */
      PUBLIC_KEY_EXCHANGE_FLAG,

  /**
   * Current state is certificates handshake.
   * If the flag is set payload should contains the handshake data
   */
      HANDSHAKE_FLAG,

  /**
  * Subsequent communication will be encrypted
  */
      UPGRADE_TO_SECURE_CHANNEL_FLAG
};
