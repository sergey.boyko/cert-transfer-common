/**
* Created by Sergey O. Boyko on 02.02.19.
*/

#pragma once

#include "stdint.h"

#pragma pack(push, 1)

/**
* Crypto protolc settings.
* Contains the main info about crypto protocol used
*/
struct CryptoProtocolSettings {
  /**
  * Protocol type.
  * Can be on of the SrdpEncryptionProtocol values.
  */
  uint8_t m_encryption_protocol_type;

  /**
  * Settings size
  */
  uint16_t m_settings_size;

  /**
  * Serialization settings.
  * Settings can be NoiseProtocolId if the Noise Protocol is used
  */
  const char *m_settings;
};

struct SynSegment {
  /**
  * Version of Security Reliable Datagram Protocol.
  * First value is major version, second value is minor version
  */
  uint16_t m_srdp_version[2];

  /**
  * Maximum segment size.
  */
  uint16_t m_max_fragment_size;

  /**
  * Retransmission timeout.
  * If the sender did not receive an ACK the packet will be sent
  * again after a retransmission timeout.
  * This value is specified in milliseconds
  */
  uint16_t m_retransmission_timeout;

  /**
  * Maximum retransmission before disconnecting
  */
  uint8_t m_max_retransmissions;

  /**
  * Fragment waiting time.
  * If the receiver received at least one packet of the fragment
  * and did not receive other packets during the fragment waiting time
  * channel will be closed.
  * This value is specified in milliseconds
  */
  uint16_t m_fragment_waiting_time;

  /**
  * Structure of crypto protocol settings
  */
  struct CryptoProtocolSettings m_crypto_protocol_settings;
};

#pragma pack(pop)
