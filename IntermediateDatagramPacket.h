/**
* Created by Sergey O. Boyko on 02.02.19.
*/

#pragma once

#include "DatagramMessage.h"

#pragma pack(push, 1)

/**
 * Medium level packet header.
 * DatagramMessage may consists of several IntermediateDatagramPacket.
 * Contains the main information about total user message
 * and the information about this packet
 */
struct IntermediateDatagramPacketHeader {
  /**
   * Header of total message.
   * The header contains information about full message
   * which could be sent in chunks
   */
  struct DatagramMessageHeader m_message_header;

  /**
   * Message identifier.
   * It used to compile full DatagramMessage
   */
  uint32_t m_message_identifier;

  /**
  * Payload length
  */
  uint16_t m_payload_length;
};

/**
* Intermediate packet in Security Reliable Datagram Protocol
*/
struct IntermediateDatagramPacket {
  /**
  * Packet header
  */
  struct IntermediateDatagramPacketHeader m_header;

  /**
  * Packet payload
  */
  const char *m_payload;
};

#pragma pack(pop)
