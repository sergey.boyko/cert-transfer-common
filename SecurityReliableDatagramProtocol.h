/**
* Created by Sergey O. Boyko on 31.01.19.
*/

#pragma once

#include <stdint.h>

#include "DatagramMessage.h"

/**
 * Security Reliable Datagram Protocol (abbreviated as SRDP)
 * is a custom datagram transfer protocol over UDP
 * which was designed specifically for "Certificate Transfer Service".
 * SRDP is message-oriented like UDP and ensures reliable like TCP
 */

#pragma pack(push, 1)

/**
 * Low level packet header.
 * The header contains information used to restore lost packets
 * and control the channel
 */
struct SrdpHeader {
  /**
  * Control flag.
  * May be one of the SrdpFlags values
  */
  uint8_t m_flag;

  /**
  * Channel identifier.
  * It uniquely identifies the channel
  * like as remote IP and port
  */
  uint16_t m_channel_identifier;

  /**
  * Sequence number of the packet
  */
  uint16_t m_sequence_number;

  /**
  * Sequence number of the packet-request
  */
  uint16_t m_ack_sequence_number;

  /**
  * Fragment offset
  */
  uint16_t m_fragment_offset;

  /**
  * Fragment length
  */
  uint16_t m_fragment_length;

  /**
  * Payload length
  */
  uint16_t m_payload_length;
};

/**
 * Low level packet in Security Reliable Datagram Protocol
 */
struct SrdpPacket {
  /**
  * Packet header
  */
  struct SrdpHeader m_header;

  /**
  * Packet payload may contains one of the following structures:
  *   - public key (public_key_exchange_flag is set)
  *   - handshake data (handshake_flag is set)
  *   - empty (other flag)
  *   - IntermediateDatagramPacket (flag is empty)
  */
  const char *m_payload;
};

#pragma pack(pop)
